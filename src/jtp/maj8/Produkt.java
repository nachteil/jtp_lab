package jtp.maj8;

/**
 * Klasa reprezentujaca produkt
 *
 * @author Stanislaw Polak
 */
class Produkt {
    public String nazwa;
    public int ilosc;
    Miasto miasto;

    public Produkt(){
        this.nazwa="";
        this.ilosc=0;
    }

    public Produkt(String nazwa,  int ilosc){
        this.nazwa=nazwa;
        this.ilosc=ilosc;
    }

    public Produkt(String nazwa,  int ilosc, Miasto miasto){
        this.nazwa=nazwa;
        this.ilosc=ilosc;
        this.miasto=miasto;
    }

    @Override
    public String toString() {
        return new StringBuilder( "Produkt: " ).append( nazwa ).append( ", ilosc: " )
                .append( ilosc ).append( ", z miasta: " ).append( miasto.toString() ).toString();
    }

}
