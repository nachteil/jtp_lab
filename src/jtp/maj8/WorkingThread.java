package jtp.maj8;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

/**
 * Created by yorg on 08.05.14.
 */
public class WorkingThread extends Thread {

    public static final ArrayList<Komputer> komputerCollection = new ArrayList<Komputer>();
    public static final ArrayList<Oprogramowanie> oprogramowanieCollection = new ArrayList<Oprogramowanie>();

    private final BlockingQueue<Produkt> queue;

    public WorkingThread(BlockingQueue<Produkt> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {

        boolean warunek = true;

        while( warunek ) {
            synchronized(this) {
                try {
                    Produkt produkt = queue.take();

                    if (produkt instanceof Komputer) {
                        komputerCollection.add((Komputer) produkt);
                    } else if (produkt instanceof Oprogramowanie) {
                        oprogramowanieCollection.add((Oprogramowanie) produkt);
                    } else if (produkt instanceof Produkt) {
                        warunek = false;
                    }
                } catch (InterruptedException e) {
                    System.out.println("Interrupted");
                }
            }
        }

        System.out.println( "Wylosowano " + komputerCollection.size() + " komputerów i "
                + oprogramowanieCollection.size() + " sztuk oprogramowania" );
    }
}
