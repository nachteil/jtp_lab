package jtp.maj8;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by yorg on 08.05.14.
 */

public class ThirdTask {

    public static void main( String ... args ) {

        BlockingQueue<Produkt> blockingQueue = new ArrayBlockingQueue<Produkt>( Integer.valueOf( args[0] ) );

        int numThreads = Integer.valueOf( args[1] );

        ThreadLocalRandom random = ThreadLocalRandom.current();

        int oprogramowanieCount = 0;
        int komputerCount = 0;

        for( int i = 0; i < numThreads; ++i ) {
            new WorkingThread( blockingQueue ).start();
        }

        infiniteLoop:
        while( 1 < 17 ) {
            int randomValue = random.nextInt( 50 );

            try {
                if( randomValue == 0 ) {
                    String nazwa = String.format( "Losowy-produkt-%s", Integer.toString( random.nextInt( 50 ) ) );
                    blockingQueue.add( new Produkt( nazwa, random.nextInt( 23 ), Miasto.losuj() ) );
                    break infiniteLoop;
                } else if( randomValue % 2 == 0 ) {
                    blockingQueue.add( Oprogramowanie.losuj() );
                    ++oprogramowanieCount;
                } else {
                    blockingQueue.add( Komputer.losuj() );
                    ++komputerCount;
                }
            } catch ( IllegalStateException e ) {

            }


        }

        System.out.println( "Wylosowano " + komputerCount + " komputerów i "
                + oprogramowanieCount + " sztuk oprogramowania" );

    }

}
