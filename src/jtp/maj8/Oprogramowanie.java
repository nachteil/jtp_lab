package jtp.maj8;

import java.util.Random;

/**
 * Klasa reprezentujaca Oprogramowanie
 *
 * @author Stanislaw Polak
 */
class Oprogramowanie extends Produkt {
    public float cena;

    public Oprogramowanie(){
        super();
        this.cena=0.0f;
    }

    public Oprogramowanie(String nazwa, int ilosc, float cena){
        super(nazwa,ilosc);
        this.cena=cena;
    }

    public Oprogramowanie(String nazwa, int ilosc, float cena,Miasto miasto){
        super(nazwa,ilosc,miasto);
        this.cena=cena;
    }

    /**
     * Losuje oprogramowanie
     *
     * @return Wylosowane oprogramowanie
     */
    public static Oprogramowanie losuj() {
	Random random = new Random();
	String nazwa = String.format("Oprogramowanie-%d",random.nextInt(9)+1);
	return new Oprogramowanie(nazwa,random.nextInt(10)+1,(random.nextFloat() * 70) + 1,Miasto.losuj());
    }
}
