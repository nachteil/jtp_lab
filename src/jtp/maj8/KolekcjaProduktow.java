package jtp.maj8;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Kolekcja produktow
 *
 * @author Stanislaw Polak
 */
class KolekcjaProduktow {

    private static List<Produkt> produkty;

    public KolekcjaProduktow(int rozmiar) {
        produkty = new ArrayList<Produkt>();
	for (int i = 0; i < rozmiar; ++i) {
            Random random = new Random();
            if (random.nextInt(2) == 0)
		produkty.add(Komputer.losuj());
	    else
		produkty.add(Oprogramowanie.losuj());
	}
    }

    public static void runThreads() {

        Object lockObject = new Object();

        for( int i = 0; i < 5; ++i ) {

            new Thread( () -> {

                ThreadLocalRandom random = ThreadLocalRandom.current();

                for( int j = 0; j < 100; ++j ) {

                    int sleepTime = random.nextInt( 2000 ) + 2000;

                    try {
                        Thread.sleep( sleepTime );
                    } catch ( InterruptedException e ) {
                        System.out.println( "Polamilo sie" );
                    }

                    int produktIndex = random.nextInt( produkty.size() );

                    Produkt randomProdukt = produkty.get( produktIndex );

                    synchronized( lockObject ) {
                        System.out.println( Thread.currentThread().getName() + ": "
                                + randomProdukt.nazwa );

                        sleepTime = random.nextInt( 500 ) + 500;

                        try {
                            Thread.sleep( sleepTime );
                        } catch ( InterruptedException e ) {
                            System.out.println( "Polamilo sie" );
                        }

                        System.out.println( Thread.currentThread().getName() + ": "
                                + randomProdukt.toString() );
                    }


                }



            } ).start();

        }
    }
}
