package jtp.maj8;

import java.util.Random;

/**
 * Klasa reprezentujaca komputer
 *
 * @author Stanislaw Polak
 */
class Komputer extends Produkt {
    public float cena_procesor;
    public float cena_dysk;

    public Komputer(){
        super();
        this.cena_procesor=0.0f;
        this.cena_dysk=0.0f;
    }

    public Komputer(String nazwa,  int ilosc,float cena_procesor, float cena_dysk){
        super(nazwa,ilosc);
        this.cena_procesor=cena_procesor;
        this.cena_dysk=cena_dysk;
    }

    public Komputer(String nazwa,  int ilosc,float cena_procesor, float cena_dysk,Miasto miasto){
        super(nazwa,ilosc,miasto);
        this.cena_procesor=cena_procesor;
        this.cena_dysk=cena_dysk;
    }

    /**
     * Losuje komputer
     *
     * @return Wylosowany komputer
     */
    public static Komputer losuj() {
	Random random = new Random();
	String nazwa = String.format("Komputer-%d",random.nextInt(9)+1);
	return new Komputer(nazwa,random.nextInt(10)+1,(random.nextFloat() * 700) + 1,(random.nextFloat() * 70) + 1,Miasto.losuj());
    }
}
