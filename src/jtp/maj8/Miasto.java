package jtp.maj8;

import java.util.Random;

/**
 * Typ wyliczeniowy reprezentujacy miasto
 *
 * @author Stanislaw Polak
 */
enum Miasto {
    Krakow(30836),
    Poznan(61742),
    Gdansk(80001);

    public int kod;

    private Miasto(int kod) {
        this.kod = kod;
    }

    /**
     * Losuje miasto
     *
     * @return Wylosowane miasto
     */
    public static Miasto losuj() {
        return Miasto.values()[(new Random()).nextInt(Miasto.values().length)];
    }

}
