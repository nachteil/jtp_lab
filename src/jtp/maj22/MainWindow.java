package jtp.maj22;

/**
 * Created by yorg on 22.05.14.
 */

import java.awt.Graphics;
import java.awt.Graphics2D;

public class MainWindow extends java.applet.Applet {


    private double theta = 0;

    class DrawingRectangle implements Runnable {

        private final Graphics2D g2;

        DrawingRectangle( Graphics g ) {
            this.g2 = (Graphics2D) g;

        }

        @Override
        public void run() {

            while( 5 < 123 ) {
                try {
                    theta += 2 * Math.PI / 10000 * 100;
                    Thread.currentThread().sleep(100);
                    repaint();
                } catch (InterruptedException e) {
                    System.out.println("Aborted");
                }
            }
        }
    }

    public void paint(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2.drawRect(120, 120, 160, 160);
        g2.rotate(this.theta, 200, 200);

        System.out.println("Wywołano metodę 'paint()'");
    }

    public void init(){
        System.out.println("Wywołano metodę 'init()'");
    }

    public void start(){
        System.out.println("Wywołano metodę 'start()'");
        DrawingRectangle drawingRectangle = new DrawingRectangle( this.getGraphics() );
        new Thread( drawingRectangle ).start();
    }

    public void stop(){
        System.out.println("Wywołano metodę 'stop()'");
    }
}
