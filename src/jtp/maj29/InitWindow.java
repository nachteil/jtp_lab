package jtp.maj29;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by yorg on 29.05.14.
 */
public class InitWindow extends Frame implements ActionListener {

    First secondWindow;
    boolean visible = false;
    Button button;

    public InitWindow() {
        super( "Okno pierwsze" );

        button = new Button( "Toggle visibility" );

        this.secondWindow = new First();
        secondWindow.setVisible( false );

        button.addActionListener( this );

        setSize( 400, 400 );

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });

        button.setSize( 100, 50 );
        button.setPreferredSize( new Dimension( 100, 50 ) );
        button.setBounds( 20, 20, 100, 50 );
        add(button);

        this.setVisible( true );
    }

    public static void main( String ... args ) {
        InitWindow window = new InitWindow();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.visible = ! this.visible;
        secondWindow.setVisible( this.visible );
    }
}
