package jtp.maj29;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by yorg on 29.05.14.
 */
public class First extends Frame {

    private MyCanvas canvas;

    private Button leftButton;
    private Button rightButton;
    private Button topButton;
    private Button bottomButton;

    public First() {
        super( "Zadanie drugie" );
        this.setSize( 500, 500 );

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });

        canvas = new MyCanvas();

        initButtons();
        addButtonsListeners();

        setLayout( new BorderLayout() );

        this.add( leftButton, BorderLayout.WEST );
        this.add( rightButton, BorderLayout.EAST );
        this.add( topButton, BorderLayout.NORTH );
        this.add( bottomButton, BorderLayout.SOUTH );

        this.add( canvas, BorderLayout.CENTER );

        // setVisible(true);
    }

    private void initButtons() {

        leftButton = new Button( "Czerwony" );
        rightButton = new Button( "Niebieski" );
        topButton = new Button( "Zielony" );
        bottomButton = new Button( "Czarny" );

    }

    private void addButtonsListeners() {

        leftButton.addActionListener( canvas );
        rightButton.addActionListener( canvas );
        topButton.addActionListener( canvas );
        bottomButton.addActionListener( canvas );

    }

    public static void main( String ... args ) {
        First window = new First();
    }

}
