package jtp.maj29;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by yorg on 29.05.14.
 */
public class MyCanvas extends Canvas implements MouseListener, ActionListener {

    private Point [] points = new Point[10];
    private int pointsCounter = 0;

    private Color color;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public MyCanvas() {
        this.color = Color.BLUE;
        addMouseListener( this );
    }

    @Override
    public void paint( Graphics g ) {
        //super.paint( g );
        Graphics2D g2d = ( Graphics2D ) g;

        System.out.println( "rysowanie" );

        for( int i = 0; i < pointsCounter; ++i ) {
            g2d.setColor( points[i].getColor() );
            g2d.fillOval( points[i].getX(), points[i].getY(), 15, 15 );
            System.out.println( "koks" );
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if( pointsCounter == 10 ) {
            return;
        }

        int x = e.getX();
        int y = e.getY();

        points[pointsCounter++] = new Point( x, y, color );
        repaint();
        System.out.println( "Widzie i słyszę" );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println( e );
        switch ( e.getActionCommand() ) {
            case "Zielony":
                color = Color.green;
                break;

            case "Czarny":
                color = Color.black;
                break;

            case "Niebieski":
                color = Color.blue;
                break;

            case "Czerwony":
                color = Color.red;
                break;

            default:
                break;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
