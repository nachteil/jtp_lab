package jtp.maj29;

import java.awt.*;

/**
* Created by yorg on 29.05.14.
*/
public class Point {

    private final int x;
    private final int y;

    private final Color color;

    Point(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }
}
