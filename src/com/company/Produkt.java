package com.company;

import javax.lang.model.element.Name;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Klasa reprezentująca produkt
 *
 * @author Stanisław Polak
 */
class Produkt {
    public String nazwa;
    public int ilość;
    Miasto miasto;

    public Produkt(){
        this.nazwa="";
        this.ilość=0;
    }

    public Produkt(String nazwa,  int ilość){
        this.nazwa=nazwa;
        this.ilość=ilość;
    }

    public Produkt(String nazwa,  int ilość, Miasto miasto){
        this.nazwa=nazwa;
        this.ilość=ilość;
        this.miasto=miasto;
    }

}
