package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Kolekcja produktów
 *
 * @author Stanisław Polak
 */
class KolekcjaProduktów {
    private List<Produkt> produkty;

    public KolekcjaProduktów(int rozmiar) {
        produkty = new ArrayList<Produkt>();
        for (int i = 0; i < rozmiar; ++i) {
            Random random = new Random();
            if (random.nextInt(2) == 0)
                produkty.add(Komputer.losuj());
            else
                produkty.add(Oprogramowanie.losuj());
        }
    }

    public void zapisz( FileWriter fw ) {

        try {
            for (Produkt produkt : produkty) {
                if( produkt instanceof Komputer ) {
                    ((Komputer) produkt).writeCSV( fw );
                } else {
                    ((Oprogramowanie) produkt).writeCSV( fw );
                }
            }
            fw.flush();
            fw.close();

        } catch ( IOException e ) {
            System.out.println( "Polamilo sie" );
            e.printStackTrace();
        } finally {

        }
    }
}
