package com.company;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by yorg on 24.04.14.
 */
public interface CSVable {
    public void writeCSV( FileWriter fileWriter ) throws IOException;
}
