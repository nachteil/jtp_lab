package com.company;

import java.util.Random;

/**
 * Typ wyliczeniowy reprezentujący miasto
 *
 * @author Stanisław Polak
 */
enum Miasto {
    Kraków(30836),
    Poznań(61742),
    Gdańsk(80001);

    public int kod;

    private Miasto(int kod) {
        this.kod = kod;
    }

    /**
     * Losuje miasto
     *
     * @return Wylosowane miasto
     */
    public static Miasto losuj() {
        return Miasto.values()[(new Random()).nextInt(Miasto.values().length)];
    }

}
