package com.company;

import java.io.*;
import java.util.Random;

/**
 * Klasa reprezentująca Oprogramowanie
 *
 * @author Stanisław Polak
 */
class Oprogramowanie extends Produkt implements CSVable {
    public float cena;

    public Oprogramowanie(){
        super();
        this.cena=0.0f;
    }

    public Oprogramowanie(String nazwa, int ilość, float cena){
        super(nazwa,ilość);
        this.cena=cena;
    }

    public Oprogramowanie(String nazwa, int ilość, float cena,Miasto miasto){
        super(nazwa,ilość,miasto);
        this.cena=cena;
    }

    /**
     * Losuje oprogramowanie
     *
     * @return Wylosowane oprogramowanie
     */
    public static Oprogramowanie losuj() {
        Random random = new Random();
        String nazwa = String.format("Oprogramowanie-%d",random.nextInt(9)+1);
        return new Oprogramowanie(nazwa,random.nextInt(10)+1,(random.nextFloat() * 70) + 1,Miasto.losuj());
    }

    @Override
    public void writeCSV( FileWriter fileWriter ) throws IOException {
        fileWriter.append( nazwa );
        fileWriter.append( "," );
        fileWriter.append( String.valueOf( ilość ) );
        fileWriter.append( "," );
        fileWriter.append( miasto.toString() );
        fileWriter.append( "," );
        fileWriter.append( String.valueOf( cena ) );
        fileWriter.append( "\n" );
    }
}
