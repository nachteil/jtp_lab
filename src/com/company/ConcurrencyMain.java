package com.company;

/**
 * Created by yorg on 08.05.14.
 */
public class ConcurrencyMain {
    public static void main( String ... args ) {

        new Thread( new RunnableProdukt() ).start();
        new Thread( new RunnableProdukt() ).start();
        new MyThread().start();
        new MyThread().start();

    }

}
