package com.company;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by yorg on 08.05.14.
 */
public class RunnableProdukt extends Produkt implements Runnable {

    private static final int N = 11;

    @Override
    public void run() {

        ThreadLocalRandom localRandom = ThreadLocalRandom.current();

        for( int i = 0; i < N; ++i ) {
            int sleepTime = localRandom.nextInt( 5000 );
            System.out.print( "(" + this.getClass().getSuperclass().getSimpleName() + ")" );
            System.out.print( " Wątek o nazwie" + Thread.currentThread().getName() );
            System.out.println( ", lulu przez " + sleepTime + " ms" );

            try {
                Thread.sleep( sleepTime );
            } catch( InterruptedException e ) {
                System.out.println( "Coś przerwało drzemkę" );
                e.printStackTrace();
                return;
            }
        }
    }
}
