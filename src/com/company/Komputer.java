package com.company;

import java.io.*;
import java.util.Random;

/**
 * Klasa reprezentująca komputer
 *
 * @author Stanisław Polak
 */
class Komputer extends Produkt implements CSVable {
    public float cena_procesor;
    public float cena_dysk;

    public Komputer(){
        super();
        this.cena_procesor=0.0f;
        this.cena_dysk=0.0f;
    }

    public Komputer(String nazwa,  int ilość,float cena_procesor, float cena_dysk){
        super(nazwa,ilość);
        this.cena_procesor=cena_procesor;
        this.cena_dysk=cena_dysk;
    }

    public Komputer(String nazwa,  int ilość,float cena_procesor, float cena_dysk,Miasto miasto){
        super(nazwa,ilość,miasto);
        this.cena_procesor=cena_procesor;
        this.cena_dysk=cena_dysk;
    }

    /**
     * Losuje komputer
     *
     * @return Wylosowany komputer
     */
    public static Komputer losuj() {
        Random random = new Random();
        String nazwa = String.format("Komputer-%d",random.nextInt(9)+1);
        return new Komputer(nazwa,random.nextInt(10)+1,(random.nextFloat() * 700) + 1,(random.nextFloat() * 70) + 1,Miasto.losuj());
    }

    @Override
    public void writeCSV( FileWriter fileWriter ) throws IOException {
        fileWriter.append( nazwa );
        fileWriter.append( "," );
        fileWriter.append( String.valueOf( ilość ) );
        fileWriter.append( "," );
        fileWriter.append( miasto.toString() );
        fileWriter.append( "," );
        fileWriter.append( String.valueOf( cena_dysk ) );
        fileWriter.append( "," );
        fileWriter.append( String.valueOf( cena_procesor ) );
        fileWriter.append( "\n" );
    }
}
